package pl.aprilapps.statbar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout barContainer = (FrameLayout) findViewById(R.id.statbar_container);
        FrameLayout legendContainer = (FrameLayout) findViewById(R.id.legend_container);
        //float[] values = new float[] {35, 21, 41, 67, 12, 4, 10, 12, 31.5f, 21, 10, 13, 12, 15, 17, 18};
        float[] values = new float[] {35, 21, 41};
        //AaStatBar statBar = AaStatBar.sortedStatBarInstance(this, values, null, true);
        AaStatBar statBar = AaStatBar.multiprogressBarInstance(this, values);
        statBar.setDisplayTitleLabel(false);
        statBar.setDisplayValueLabel(false);

        barContainer.addView(statBar);
        try {
            statBar.drawStatBar(barContainer);
            statBar.drawLegendView(legendContainer, LinearLayout.VERTICAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}

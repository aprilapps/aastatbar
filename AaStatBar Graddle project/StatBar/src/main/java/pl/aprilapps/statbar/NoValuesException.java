package pl.aprilapps.statbar;

/**
 * Created by jacek on 14.01.2014.
 */
public class NoValuesException extends  Exception {

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }

    @Override
    public String getMessage() {
        return "Values must be set using setValues() before calling invalidate.";
    }
}

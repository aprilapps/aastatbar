package pl.aprilapps.statbar;

/**
 * Created by jacek on 14.01.2014.
 */
public class NotEnoughColorsException extends  Exception {

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }

    @Override
    public String getMessage() {
        return "Colors array must be at least the length ov values array + 1 (last color is for summarized 'Other' value.";
    }
}

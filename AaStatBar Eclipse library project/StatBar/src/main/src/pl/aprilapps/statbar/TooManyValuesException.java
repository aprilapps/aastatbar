package pl.aprilapps.statbar;

/**
 * Created by jacek on 14.01.2014.
 */
public class TooManyValuesException extends  Exception {

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }

    @Override
    public String getMessage() {
        return "Stat bar with default array of colors, can only take up to 11 values.";
    }
}

package pl.aprilapps.statbar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by jacek on 14.01.2014.
 */
public class AaStatBar extends LinearLayout {

    private final int MAX_VALUES = 10;

    Float[] values;
    float exceedingValue;
    int[] colors;
    boolean displayValueLabel = true;
    boolean displayTitleLabel = true;

    /**
     * Returns instance of StatBar
     * @param context
     * @param valuesArray Array of values o present in the stat bar. Values will be presented in sorted order.
     * @param max Maximum number of values presented (Other value excluded). If values array is bigger than this number, all exceeding values will be summarized to "Other" value. If null, max will be calculated automatically.
     * @param descending Sorting order, if set to false, values will be sorted ascending
     * @return Instance of StatBar
     */
    public static AaStatBar sortedStatBarInstance(Context context, float[] valuesArray, Integer max, boolean descending) {
        AaStatBar statBar = new AaStatBar(context);

        statBar.values = new Float[valuesArray.length];
        for (int i = 0; i < valuesArray.length; i++) statBar.values[i] = Float.valueOf(valuesArray[i]);

        if (!descending) Arrays.sort(statBar.values);
        else Arrays.sort(statBar.values, Collections.reverseOrder());

        if (max != null) {
            if (max > statBar.values.length && statBar.values.length > statBar.MAX_VALUES) max = statBar.MAX_VALUES;
            else if (max > statBar.values.length) max = statBar.values.length;
        }
        else if (statBar.values.length > statBar.MAX_VALUES) max = statBar.MAX_VALUES;
        else max = statBar.values.length;

        if (statBar.values.length > statBar.MAX_VALUES) {
            Float[] presentedValues = Arrays.copyOf(statBar.values, statBar.MAX_VALUES);
            Float[] exceedingValues = Arrays.copyOfRange(statBar.values, statBar.MAX_VALUES + 1, statBar.values.length);
            statBar.values = presentedValues;
            statBar.exceedingValue = 0;
            for (int i = 0; i < exceedingValues.length; i++) {
                statBar.exceedingValue += exceedingValues[i];
            }
        }

        statBar.setDefaultColors();

        return statBar;
    }

    /**
     * Returns instance of StatBar working as multiprogress bar
     * @param context
     * @param valuesArray Array of values o present in the multiprogress bar. Those values will be presented in order of this array.
     * @return Instance of multiprogress bar
     */
    public static AaStatBar multiprogressBarInstance(Context context, float[] valuesArray) {
        AaStatBar statBar = new AaStatBar(context);

        statBar.values = new Float[valuesArray.length];
        for (int i = 0; i < valuesArray.length; i++) statBar.values[i] = Float.valueOf(valuesArray[i]);
        statBar.setDefaultColors();

        return statBar;
    }

    private AaStatBar(Context context) {
        super(context);
    }

    private void setDefaultColors() {
        colors = new int[11];
        colors[0] = getResources().getColor(R.color.chart_color_3);
        colors[1] = getResources().getColor(R.color.chart_color_4);
        colors[2] = getResources().getColor(R.color.chart_color_5);
        colors[3] = getResources().getColor(R.color.chart_color_2);
        colors[4] = getResources().getColor(R.color.chart_color_1);
        colors[5] = getResources().getColor(R.color.chart_color_6);
        colors[6] = getResources().getColor(R.color.chart_color_7);
        colors[7] = getResources().getColor(R.color.chart_color_8);
        colors[8] = getResources().getColor(R.color.chart_color_9);
        colors[9] = getResources().getColor(R.color.chart_color_10);
        colors[10] = getResources().getColor(R.color.chart_color_11);
    }

//    /**
//     * Sets the values to be presented. Sorting is not necessary, will be applied automatically.
//     * @param valuesArray Array of values o present in the stat bar
//     * @param max Maximum number of values presented (Other value excluded). If values array is bigger than this number, all exceeding values will be summarized to "Other" value. If null, max will be calculated automatically.
//     */
//    public void setValues(float[] valuesArray, Integer max) {
//
//    }

    /**
     * Draws the stat bar nto specified container
     * @param containerView Container for a stat bar
     * @throws NoValuesException
     * @throws TooManyValuesException
     */
    public void drawStatBar(ViewGroup containerView) throws NoValuesException, TooManyValuesException {
        if (values == null) throw new NoValuesException();

        LinearLayout rootView = new LinearLayout(getContext());
        LayoutInflater li = LayoutInflater.from(this.getContext());

        Float[] fullArray = getFullArray();
        if (fullArray.length > colors.length) throw new TooManyValuesException();

        for (int i = 0; i < fullArray.length; i++) {
            LinearLayout statLayout = (LinearLayout) li.inflate(R.layout.view_stat, this, false);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, getWeight(i));
            statLayout.setLayoutParams(params);            

            TextView valueLabel = (TextView) statLayout.findViewById(R.id.value_label);
            TextView titleLabel = (TextView) statLayout.findViewById(R.id.title_label);

            if (displayValueLabel) valueLabel.setText(getPercentString(i));
            else valueLabel.setVisibility(GONE);
            if (displayTitleLabel) titleLabel.setText(getBarItemTitleString(i));
            else titleLabel.setVisibility(GONE);

            statLayout.setBackgroundColor(getColor(i));

            rootView.addView(statLayout);
        }
        containerView.addView(rootView);
    }

    /**
     * Draws legend into the specified container layout.
     * @param containerView Container layout
     * @param orientation Orientation constant, might be LinearLayout.HORIZONTAL or LinearLayout.VERTICAL
     */
    public void drawLegendView(ViewGroup containerView, int orientation) {
        LinearLayout rootView = new LinearLayout(getContext());
        rootView.setOrientation(orientation);
        LayoutInflater li = LayoutInflater.from(this.getContext());

        Float[] fullArray = getFullArray();
        for (int i = 0; i < fullArray.length; i++) {
            LinearLayout legendItem = (LinearLayout) li.inflate(R.layout.view_legend_item, rootView, false);
            FrameLayout marker = (FrameLayout) legendItem.findViewById(R.id.mark_view);
            TextView label = (TextView) legendItem.findViewById(R.id.label);
            label.setText(getBarItemTitleString(i));
            marker.setBackgroundColor(getColor(i));
            rootView.addView(legendItem);
        }

        containerView.addView(rootView);
    }

    private Float[] getFullArray() {
        Float[] copy = Arrays.copyOf(values, values.length);
        if (exceedingValue > 0) {
            Float[] fullArray = new Float[copy.length + 1];
            for (int i = 0; i < copy.length; i++) {
                fullArray[i] = copy[i];
            }
            fullArray[copy.length] = exceedingValue;
            return fullArray;
        } else{
            return copy;
        }
    }

    private float getWeight(int index) {
        Float[] fullArray = getFullArray();
        float sum = getSum();
        float value = fullArray[index];
        float weight = value/sum;
        return weight;
    }

    private int getColor(int index) {
        return colors[index];
    }

    /**
     *
     * @param colors Array of colors resources. Can't be bigger than 11, otherwise it will be trimmed. The last element is "Other" summarized value color. Colors will be displayed in orders, values are sorted.
     */
    public void setColors(int[] colors) throws NotEnoughColorsException {
        if (colors.length <= values.length) throw new NotEnoughColorsException();
        if (colors.length > MAX_VALUES + 1) this.colors = Arrays.copyOf(colors, MAX_VALUES + 1);
        else this.colors = colors;
    }

    private float getSum() {
        float sum = 0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        sum += exceedingValue;
        return sum;
    }

    protected String getOtherLabelTitle() {
        return "Other";
    }

    protected LinearLayout getStatView(int index) {
        return (LinearLayout) this.getChildAt(index);
    }

    protected TextView getValueLabel(int index) {
        LinearLayout statView = getStatView(index);
        return (TextView) statView.findViewById(R.id.value_label);
    }

    protected TextView getTitleLabel(int index) {
        LinearLayout statView = getStatView(index);
        return (TextView) statView.findViewById(R.id.title_label);
    }

    public Float getValue(int index) {
        return values[index];
    }

    /**
     * @param index Index of value
     * @return String displayed as a value
     */
    protected String getBarItemTitleString(int index) {
        Float[] fullArray = getFullArray();

        if (index < fullArray.length - 1 || exceedingValue == 0) {
            return "Item " + index;
        }
        else if (exceedingValue > 0) {
            return getOtherLabelTitle();
        }
        else
            return null;
    }

    /**
     * @param index Index of value
     * @return Title for legent item. Same as bar item by default.
     */
    protected  String getLegendItemTitleString(int index) {
        return getBarItemTitleString(index);
    }

    /**
     * @param index Index of value
     * @return String displayed as percent
     */
    protected String getPercentString(int index) {
        int percent = Math.round(getValue(index));
        return Integer.toString(percent) + "%";
    }

    public void setDisplayValueLabel(boolean displayValueLabel) {
        this.displayValueLabel = displayValueLabel;
    }

    public void setDisplayTitleLabel(boolean displayTitleLabel) {
        this.displayTitleLabel = displayTitleLabel;
    }



}
